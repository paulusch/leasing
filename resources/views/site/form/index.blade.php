@extends('layout.layout')
@section('content')

<!-- SECTION IMAGE FULLSCREEN -->
<section class="halfscreen parallax" style="background-image:url({{asset('assets/images/leasing.jpg')}}); background-position:center center; background-size:cover;" data-stellar-background-ratio="0.0">
	<div class="container-fluid">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-dark">
				<h2 class="m-b-0 text-uppercase text-lg"  data-animation="fadeInDown" data-animation-delay="500">Leasing Online</h2>	
				<h3 class="text-uppercase" data-animation="fadeInDown" data-animation-delay="1000">Registration Of Interest</h3>				
			</div>
		</div>
	</div>
</section>
<!-- END: SECTION IMAGE FULLSCREEN -->

<style>
	.label-leasing{
		font-size: 100%;
		border-radius: 6px;
		padding: 15px 20px;
	}

	.f-text{color: #bbb;font-size: 11px;}

	.chosen-container-multi .chosen-choices li.search-field input[type="text"] {
		font-size:14px;
		color:#bbb;
		font-weight:500;
	}
</style>

<!-- FORM LEASING -->
<section class="p-b-20 p-t-50">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="list-inline">
					<li><h4 class="text-uppercase m-b-30">Tanggal Pengisian<br><i class="f-text">Date Of Entry</i></h4></li>
					<li><span class="label label-info label-leasing" style="position: relative;top: -11px;">{{ date('d/m/Y') }}</span></li>
				</ul>
			</div>
		</div>

		<div class="row">
      <form class="submit-online-leasing" action="{{ URL::to('submit-online-form') }}" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form-number" id="form-number" value="{{ str_random(50) }}">
			{{ csrf_field() }}
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="upper" for="brand">Merek /<br><i class="f-text">Brand</i></label>
							<input type="text" class="form-control required" name="brand" id="brand-name" aria-required="true" placeholder="contoh: Chicken Pan" maxlength="50">
							<span class="error" id="brand"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="product">Produk / Jenis Layanan<br><i class="f-text">Product / Service</i></label>
							<input type="text" class="form-control required " name="product" aria-required="true" placeholder="contoh: Fast Food" maxlength="50">
							<span class="error" id="product"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="business">Cakupan Bisnis<br><i class="f-text">Business Chain</i></label>
							{!! Form::select('bs_chain', $list_bschain, '', $attributes=['class'=>'form-control required']) !!}
							<span class="error" id="bs_chain"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="current">Jumlah Toko<br><i class="f-text">Current Store at / Branch</i></label>
							<input type="text" class="form-control required " name="branch" aria-required="true" maxlength="100">
							<span class="error" id="branch"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="theme">Kategori Retail Umum <br><i class="f-text">Category</i></label>
							{!! Form::select('theme', $list_category, '', $attributes=['class'=>'form-control required theme-options']) !!}
							<span class="error" id="theme"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="class">Kategori Retail Spesifik<br><i class="f-text">Retail Category</i></label>
							<select class="form-control class-options required" name="class">
								<option value="">Select One</option>
							</select>
							<span class="error" id="class"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="area">Luas Area Yang Dibutuhkan<br><i class="f-text">Area Required</i></label>
							<div class="row">
								<div class="col-sm-4">
									<input type="text" class="form-control required number" name="area1" maxlength="3" placeholder="m2" aria-required="true">
								</div>
								<div class="col-sm-2">
									<h6 class="text-uppercase text-center m-t-15">To</h6>
								</div>
								<div class="col-sm-4">
									<input type="text" class="form-control required number" name="area2" maxlength="3" placeholder="m2" aria-required="true">
								</div>
								<span class="error" id="area1"></span>
								<span class="error" id="area2"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="upper" for="target">Target Pasar<br><i class="f-text">Target Market</i></label>
							<input type="text" class="form-control required " name="target" aria-required="true" placeholder="contoh: Semua Kalangan / Semua Umur" maxlength="150">
							<span class="error" id="target"></span>
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-md-5 col-md-offset-2">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="upper" for="company">Perusahaan / Pemilik<br><i class="f-text">Company / Owner</i></label>
							<input type="text" class="form-control required" name="company" aria-required="true" placeholder="contoh: PT ABC / Silvia" maxlength="50">
							<span class="error" id="company"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="group">Grup<br><i class="f-text">Group</i></label>
							<input type="text" class="form-control required " name="group" aria-required="true" placeholder="" maxlength="50">
							<span class="error" id="group"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="contact">Nama<br><i class="f-text">Contact Person</i></label>
							<input type="text" class="form-control required" name="contact_person" aria-required="true" placeholder="contoh: Silvia" maxlength="50">
							<span class="error" id="contact_person"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="target">Alamat<br><i class="f-text">Address</i></label>
							<input type="text" class="form-control required " name="address" aria-required="true" placeholder="contoh: Kelapa Nias 4 No. 22, Jakarta Utara" maxlength="250">
							<span class="error" id="address"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="phone">Telepon<br><i class="f-text">Phone</i></label>
							<input type="text" class="form-control required" name="phone" aria-required="true" placeholder="contoh: 021 12345689 / 0812 123456789" maxlength="40">
							<span class="error" id="phone"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="email">Email</label>
							<input type="email" class="form-control required email" name="email" aria-required="true" placeholder="contoh: Silvia@yahoo.com" maxlength="80">
							<span class="error" id="email"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="location">Lokasi Yang Dipilih<br><i class="f-text">Preferred Location</i></label>
							{!! Form::select('location[]', $list_location, '', $attributes=['class'=>'chosen-select required','multiple'=>'multiple','aria-required'=>true ]) !!}
							<span class="error" id="location"></span>
						</div>
						<div class="form-group">
							<label class="upper" for="note">Keterangan<br><i class="f-text">Note</i></label>
							<textarea class="form-control required" name="note" rows="7" aria-required="true" placeholder="contoh : tahun 2017 akan membuka 5 toko / akan siap buka pada bulan Desember 2017 / Franchise / Master Francise / toko cabang ke-2 dan seterusnya"></textarea>
							<span class="error" id="note"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
        <div class="form-group text-center">
          <label class="upper" for="target">Proposal Bisnis & Kebutuhan Teknis<br><i class="f-text">Attachment</i>&nbsp&nbsp&nbsp<a type="button" data-container="body" data-toggle="popover" data-placement="right" title="Format Proposal :" data-content="<ul><li>Contact Person (Alamat, No. Tlp, Email)</li><li>Brand / Merk<br>Jenis, Foto Beserta Harga Produk</li><li>Luas Area Yang Dibutuhkan</li><li>Konsep Design Toko / Counter</li><li>Lokasi Toko / Counter Yang Sudah Ada Beserta Foto</li><li>Upload Video Maximal 50MB</li></ul>"><i class="f-text" style="color:#5D9CEC;">(Format Proposal</i></a>&nbsp&&nbsp<a type="button" data-container="body" data-toggle="popover" data-placement="right" title="Kebutuhan Teknis :" data-content="<ul><li>Kebutuhan listrik</li><li>Kebutuhan AC</li><li>Titik air bersih / air kotor</li><li>Gas</li><li>Exhaust</li><li>Kebutuhan Teknis lainnya</li></ul>"><i class="f-text" style="color:#5D9CEC;"> Technical Requirement)</i></a></label>
          <div id="upload-attach" class="fallback dropzone">
			<h5 class="text-center font-lg visible-xs-block visible-sm-block visible-lg-block" id="done-upload">
				Tarik/Geser Satu atau Beberapa File Kesini Untuk Upload atau klik
			</h5>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          {!! Form::captcha($attributes) !!}
        </div>
				<div class="col-md-12">
					<div class="form-group text-center">
						<button class="button yellow button-3d rounded effect icon-left" type="submit" id="form-submit"><span><i class="fa fa-paper-plane"></i>Save & Submit</span></button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</section>

<!-- END : FORM LEASING -->
@stop