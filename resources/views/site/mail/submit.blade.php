<p>Dear Sir/Madam, </p>

<br><br>
<p>Thank you for your interest to lease and be a tenant of Summarecon Malls. We will review your proposal and do our best to provide a response to your submission.</p>
<p>Please note that this inquiry does not constitute any commitment from us to provide you with space. However, we shall maintain your information in our database.</p>
<p>We look forward to future cooperation with you, and wishing you and your company every success in the future.</p>
<br>
<br>
<br>
Yours sincerely,<br><br>
Leasing Department of Summarecon
<hr>
<br>
<br>

<p>Dengan hormat,</p>

<br><br>
<p>Kami ucapkan terima kasih atas minat Bapak/Ibu untuk membuka usaha di Mal Summarecon. Kami akan meninjau proposal Anda dan berusaha secepatnya untuk memberikan tanggapan.</p>
<p>Perlu kami informasikan bahwa formulir pengajuan yang Anda kirimkan bukan merupakan komitmen dari kami untuk memberi Anda ruang sewa. Namun, informasi Anda akan tersimpan dalam database kami.</p>
<p>Semoga kita dapat bekerjasama di masa mendatang dan kami mengharapkan kesuksesan bagi Anda serta perusahaan Anda.</p>
<br>
<br>
<br>
Yours sincerely,<br><br>
Departemen Leasing Summarecon