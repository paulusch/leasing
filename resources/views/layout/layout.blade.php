<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	<link rel="shortcut icon" href="images/favicon.png">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Summarecon - Leasing Online</title>

	<!-- Bootstrap Core CSS -->
	<link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('assets/vendor/animateit/animate.min.css')}}" rel="stylesheet">

	<!-- Vendor css -->
	<link href="{{asset('assets/vendor/sweatalert/sweetalert2.css')}}" rel="stylesheet">

	<!-- Template base -->
	<link href="{{asset('assets/css/theme-base.css')}}" rel="stylesheet">

	<!-- Template elements -->
	<link href="{{asset('assets/css/theme-elements.css')}}" rel="stylesheet">	
	
	<!-- Responsive classes -->
	<link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">

	<!-- Template color -->
	<link href="{{asset('assets/css/color-variations/orange.css')}}" rel="stylesheet" type="text/css" media="screen" title="blue">

	<!-- Choosen JS -->
	<link href="{{asset('assets/css/choosen/chosen.min.css')}}" rel="stylesheet" type="text/css" media="screen" title="blue">

	<!-- LOAD GOOGLE FONTS -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

	<!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" type="text/css" href="{{('assets/css/custom.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{('assets/vendor/dropzone/dropzone.min.css')}}" media="screen" />

    <!--VENDOR SCRIPT-->
    <script src="{{asset('assets/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
	<script src="{{asset('assets/vendor/plugins-compressed.js')}}"></script>
	<script src="{{asset('assets/vendor/dropzone/dropzone.min.js')}}"></script>

</head>

<body class="wide">
	<!-- WRAPPER -->
	<div class="wrapper">

		@yield('content')

	
		<!-- FOOTER -->
		<footer id="footer" class="background-grey">
		    <div class="footer-content">
		        <div class="container">
		            <div class="row">
		            	 <div class="col-md-3">
		                   <img src="{{asset('assets/images/sa logo fullcolor.svg')}}" class="img-responsive">
		                </div>

		                <div class="col-md-3">
		                   <h4 class="widget-title text-uppercase text-dark">PT Summarecon Agung Tbk</h4>
		                    <p class="grey m-b-0">Plaza Summarecon Agung<br>
		                    Jl. Perintis Kemerderkaan No.42<br>
		                    Kayu Putih, Jakarta Timur 13210, Indonesia</p>
		                    <span class="color_blue">Open In Google Maps</span>
		                    <p class="m-t-30 m-b-0">Tel +6221 471 4567<br>
		                    Fax +6221 489  2976</P>
		                    <span  class="color_blue">career.summarecon.com</span>
		                </div>

		                <div class="col-md-3">
		                    <div class="widget clearfix widget-categories">
		                        <h4 class="widget-title text-uppercase text-dark">Grup Website</h4>
		                        <ul class="list list-arrow-icons">
		                            <li> <a href="http://www.summareconkelapagading.com/home.asp" target="_blank">Summareconkelapagading.com</a> </li>
		                            <li> <a href="http://www.summareconserpong.com" target="_blank">Summareconserpong.com</a> </li>
		                            <li> <a href="http://www.summareconbekasi.com" target="_blank">Summareconbekasi.com</a> </li>
		                            <li> <a href="http://www.summareconbandung.com" target="_blank">Summareconbandung.com</a> </li>
		                            <li> <a href="http://www.summareconkarawang.com" target="_blank">Summareconkarawang.com</a> </li>
		                        </ul>
		                    </div>
		                </div>

		                <div class="col-md-3">
		                    <div class="widget clearfix widget-categories m-t-25">
		                    	<h4 class="widget-title text-uppercase text-dark"></h4>
		                        <ul class="list list-arrow-icons">
		                            <li><a href="http://www.malkelapagading.com" target="_blank">Malkelapagading.com</a></li>
		                            <li><a href="http://www.malserpong.com" target="_blank">Malserpong.com</a></li>
		                            <li><a href="http://www.malbekasi.com" target="_blank">Malbekasi.com</a></li>
		                            <li><a href="http://www.samastabali.com" target="_blank">Samastabali.com</a></li>
		                            <li><a href="http://www.jfff.info" target="_blank">Jfff.info</a></li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>  
		    <div class="copyright-content p-t-10 p-b-10" style="min-height: 60px;">
		        <div class="container">
		            <div class="row">
		                <div class="copyright-text text-center"> Copyright 2017 PT Summarecon Agung Tbk. All Right Reserved</div>
		            </div>
		        </div>
			</div>
		</footer>
		<!-- END: FOOTER -->

	</div>
	<!-- END: WRAPPER -->
	<div id="loading"></div>

	<!-- GO TOP BUTTON -->
	<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

	<!-- Theme Base, Components and Settings -->
	<script src="{{asset('assets/js/theme-functions.js')}}"></script>
	<script type="text/javascript">var base_url="{{ URL::to('') }}"</script>
	<!-- Custom js file -->
	<script src="{{asset('assets/js/custom.js')}}?{{ date('ymdHis') }}"></script>
	<script type="text/javascript">
	@if(Session::has('submit-sukses'))
	    swal({
	        title: 'Success',
	        type: 'success',
	        html: 'Data Berhasil Disimpan !'
	    });
	    <?php Session::forget('submit-sukses') ?>
	@endif
	</script>

</body>
</html>
