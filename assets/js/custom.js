$(function(){
	
    overlay('loading');
    $('#loading').hide();

    $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })

	$('.theme-options').change(function(){
		var value=$(this).val();
		var token = $("input[name='_token']").val();
	    $.ajax({
            url: base_url+"/get-class",
            data: {__theme: value, _token: token},
            type: 'post',
            headers: {
                "X-CSRF-TOKEN": token
            },
            success: function(data) {
            	$('.class-options option').remove();
            	$('.class-options').append(data);
            },
            error: function(xhr,status,error) {

            }
        })
	})

    $('#form-submit').click(function(){
        $('#loading').show();
    })
    
    $('.submit-online-leasing').validate({
        submitHandler: function (form){
            //Validate Recaptcha 
            $('#loading').show();
            var formData=new FormData(form);
            $.ajax({
                type: "post",
                url: base_url+"/submit-online-form",
                data: formData,
                dataType: "json",
                contentType: false,
                processData: false,
                success: function (res) {
                    if(res.status=='1') {
                        window.location.assign(base_url);
                    } else {                        
                        $('#loading').hide();
                        grecaptcha.reset();
                        swal({'title':'Proses Input Gagal','type':'error','html':res.msg});
                    }                    
                },
                error: function (param) {  
                    $('#loading').hide();
                    grecaptcha.reset();
                    swal({'title':'Proses Input Gagal','type':'error','html':param});
                }
            });                 
            //form.submit();
        },
        invalidHandler: function() {
            $('#loading').hide();
        },    
    });

    $(document).on('click','.add-new-attachment-btn',function(){
        var html='';
        var number=rand_number(1,9999);
        html+='<div id="div-'+number+'">';
        html+='<label class="upper" for="target">Proposal Bisnis<br><i class="f-text">Attachment</i></label>';
        html+='<input type="file" class="form-control new-attachment required" name="attachment[]" aria-required="true" accept=".jpg,.jpeg,.png,.pdf,.ppt,.pptx,.doc,.docx">';
        html+='<label class="delete-attachement danger" data-id="'+number+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Attachment</label>';
        html+='</div>';

        $('.proposal-bisnis').append(html);
    })

    $(document).on('click','.delete-attachement',function(){
        if(confirm('Delete Selected Attachment ?')) {
            var id=$(this).attr('data-id');
            $('#div-'+id).remove();
        }
    })

    $(".chosen-select").chosen({
        width: "100%"
    });

    $('.theme-options').trigger('change');

    setTimeout(function(){ 
        $('.g-recaptcha > div').css('width','100%')
    }, 4000);

    disableForm()

    // $('#form-number').val(formNumber(30))
    Dropzone.options.myAwesomeDropzone = false;
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#upload-attach", { 
        paramName: "file",
        url: base_url+"/upload-attachement",
        addRemoveLinks : true,
        uploadMultiple: false,
        maxFilesize: 40,
        timeout: 180000,
        // acceptedFiles: 'image/*,video/mp4,application/pdf,application/x-msdownload',
        acceptedFiles: '.mp4,.jpg,.jpeg,.png,.pdf,.ppt,.pptx,.doc,.docx',
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Tarik/Geser Satu atau Beberapa File Kesini <span class="font-xs">Untuk Upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Atau Klik)</h4></span>',
        dictResponseError: 'Error uploading file!',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        sending: function(file, xhr, formData) {
            var brandName = $('#brand-name').val();
            var formNumber = $('#form-number').val();
            formData.append("form_number", formNumber);
            formData.append("uuid", file.upload.uuid);
            formData.append("brand", brandName);
            $('#form-submit').text('Silahkan menunggu hingga proses upload selesai untuk Submit')
        },
        success: function(file, response) {
            //alert(response);
            if(response.status=='1') {
                console.log(file)
                // $('#form-submit').text('Save & Submit')
                // $('#form-submit').removeAttr('disabled')
                // var count= myDropzone.getAcceptedFiles().length;
                var totalPending = myDropzone.getUploadingFiles().length
                console.log(totalPending)
                if(totalPending>0) {
                    $('#form-submit').attr('disabled','disabled')
                    $('#form-submit').text('Silahkan menunggu hingga proses upload selesai untuk Submit')
                } else {
                    $('#form-submit').text('Save & Submit')
                    $('#form-submit').removeAttr('disabled')
                }
            }
        }
    });
    myDropzone.on("addedfile", function(file) {
        console.log(file)
        var _ref;
        var brandName = $('#brand-name').val().trim();
        if(brandName=='') {
            swal({'title':'Proses Upload Gagal','type':'error','html':'Merek / Brand Harus Diisi !'});
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
    });
    myDropzone.on("removedfile", function(file) {
        // console.log(file.upload.uuid)
        const uuid = file.upload.uuid
        const token = $('meta[name="csrf-token"]').attr('content')
        const formNumber = $('#form-number').val();
        $.ajax({
            type: "post",
            url:  base_url+"/delete-attachement",
            data: {
                uuid: uuid, form_number: formNumber, _token: token
            },
            success: function() {
                var count= myDropzone.getAcceptedFiles().length;
                console.log(count)
                if(count<=0) {
                    $('#form-submit').text('Attachment harus diupload minimal 1 file')
                    $('#form-submit').attr('disabled','disabled')
                }
            }
        });
    });
     
    $('#loading').hide();
})

function disableForm()
{
    $('#form-submit').text('Attachment harus diupload minimal 1 file')
    $('#form-submit').attr('disabled','disabled')
}

function overlay(elm)
{
    var maskHeight = $(document).height();
    var maskWidth = $(document).width();
    $('#'+elm)
    .css({'width':maskWidth,'height':maskHeight,'position':'fixed','z-index':'9000','background-color':'#FFF','top':'0px','left':'0px'})
    .empty()
    .append('<center><img src="'+base_url+'/images/ajax-loader.gif" id="imgloader"></center>');
    $('#imgloader').css({'top':(($(window).height())/2),'left':((maskWidth/2)-($('#imgloader').width() / 2)),'position':'absolute'});    
    $('#'+elm).fadeIn(1000);   
    $('#'+elm).fadeTo('slow',0.6); 
}

function rand_number(min,max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function formNumber(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 } 