<?php
namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\TempUpload;
use Illuminate\Http\Request;
use Validator;
use Mail;
use DB;

class FormController extends Controller{
	
	protected $_request;
	public function __construct(Request $request)
	{
		$this->_request=$request;
	}

	public function index()
	{
		$dataChain=Form::getBisChain([])->get();
		$list_bschain['']='Select One';
		foreach ($dataChain as $row) {
			$list_bschain[$row->KD_BNS_CHAIN]=strip_tags($row->NM_BNS_CHAIN);
		}

		$dataCategory=Form::getCategory([])->get();
		$list_category['']='Select One';
		foreach ($dataCategory as $row) {
			$list_category[$row->THEME_CD]=$row->DESCS;
		}

		// $dataGroup=Form::getGroup([])->get();
		// $list_group['']='Select One';
		// foreach ($dataGroup as $row) {
		// 	$list_group[$row->GRUP_ID]=$row->NAMA;
		// }

		$dataLocation=Form::getLocation([])->get();
		//$list_location['']='Select One';
		foreach ($dataLocation as $row) {
			$list_location[$row->ID_LOCATION_PREFERRED]=$row->NOTES;
		}

		$data['list_bschain']=$list_bschain;
		$data['list_category']=$list_category;
		//$data['list_group']=$list_group;
		$data['list_location']=$list_location;

		return view('site.form.index',$data);
	}

	public function getClass()
	{
		$theme=trim($this->_request->input('__theme'));
		$dataDetail=Form::getCategoryDetail(['THEME_CD'=>$theme,'FLAG_AKTIF'=>'A'])->get();
		$option='<option value="">Select One</option>';
		foreach ($dataDetail as $row) 
		{
			$option.='<option value='.$row->CLASS_CD.'>'.$row->DESCS.'</option>';
		}

		return $option;
	}

	public function submit()
	{
		$brand=trim($this->_request->input('brand'));
		$product=trim($this->_request->input('product'));
		$bs_chain=trim($this->_request->input('bs_chain'));
		$branch=trim($this->_request->input('branch'));
		$theme=trim($this->_request->input('theme'));
		$class=trim($this->_request->input('class'));
		$area1=trim($this->_request->input('area1'));
		$area2=trim($this->_request->input('area2'));
		$target=trim($this->_request->input('target'));

		$company=trim($this->_request->input('company'));
		$group=trim($this->_request->input('group'));
		$contact_person=trim($this->_request->input('contact_person'));
		$address=trim($this->_request->input('address'));
		$phone=trim($this->_request->input('phone'));
		$email=trim($this->_request->input('email'));
		$location=$this->_request->input('location');
		$note=trim($this->_request->input('note'));
		$tanggal=gmdate("Y-m-d H:i:s",time()+60+60*7);

    //Validaasi Data
    $rules=['g-recaptcha-response'=>'required|captcha'];
    $validation=Validator::make($this->_request->all(),$rules);
    if($validation->fails()) {
      return response()->json(['status'=>'0','msg'=>'Silahkan Centang Checkbox "Im Not a Robot" Diatas !']);
    }
    // Validasi Attachemtn
    $formNumber = trim($this->_request->input('form-number'));
    $recFiles = TempUpload::where(['form_number'=>$formNumber,'is_updated'=>'0'])->count();
    if($recFiles<=0) {
      return response()->json(['status'=>'0','msg'=>'Silahkan Upload Minimal 1 Attachment !']);
    }

    //Validasi Data Double
    $paramField=[
      'PT_GROUP'=>$group,
      'KD_BNS_CHAIN'=>$bs_chain,
      'THEME_CD'=>$theme,
      'CLASS_CD'=>$class,
      'PT_TRADE_NAME'=>$brand,
      'PT_COMPANY_OWNER'=>$company,
      'PT_PRODUCT'=>$product,
      'PT_CONTACT_PERSON'=>$contact_person,
      'PT_STORE'=>$branch,
      'PT_ADDRESS_1'=>$address,
      'PT_PHONE'=>$phone,
      'PT_EMAIL'=>$email,
      'PT_AREA'=>$area1,
      'PT_AREA2'=>$area2,
      'PT_CUSTOMER_TARGET'=>$target,
    ];
    $checkDouble=Form::where($paramField)->first();
    if($checkDouble) {
      return response()->json(['status'=>'0','msg'=>'Data Sudah Pernah Diinput Sebelumnya !']);
    }

		try 
		{
      DB::beginTransaction();

      $dataInsert=[
        'PT_GROUP'=>$group,
        'KD_BNS_CHAIN'=>$bs_chain,
        'THEME_CD'=>$theme,
        'CLASS_CD'=>$class,
        'PT_ENTRY_POST'=>$tanggal,
        'PT_TRADE_NAME'=>$brand,
        'PT_COMPANY_OWNER'=>$company,
        'PT_PRODUCT'=>$product,
        'PT_CONTACT_PERSON'=>$contact_person,
        'PT_STORE'=>$branch,
        'PT_ADDRESS_1'=>$address,
        'PT_PHONE'=>$phone,
        'PT_EMAIL'=>$email,
        'PT_AREA'=>$area1,
        'PT_AREA2'=>$area2,
        'PT_CUSTOMER_TARGET'=>$target,
        'PT_STATUS_KET'=>$note,
        // 'PT_TOKO_IMG'=>$aaa,
        // 'PT_SOURCE'=>$aaa,
        'PT_FLAG_AKTIF'=>'Y',
        'PT_USER_ENTRY'=>'ONLINE',
        'PT_TGL_ENTRY'=>$tanggal,
        'PT_TGL_UPDATE'=>$tanggal,
        'TMP_FORM_NUMBER'=>trim($this->_request->input('form-number'))
      ];
			$tableId = Form::updateData('insert_id',$dataInsert);	

      // Save Data Attachment
      $recTempFiles = TempUpload::where(['form_number'=>$formNumber,'is_updated'=>'0'])->get();
      foreach ($recTempFiles as $item) {
        $dataInsert=[
          'PT_ID'=>$tableId,
          'ATTACHMENT'=>$item->file_name,
          'AKTIF_YN'=>'Y',
          'USER_ENTRY'=>'ONLINE',
          'TGL_ENTRY'=>$tanggal,
          'TGL_UPDATE'=>$tanggal,
          'TEMP_ID'=>$item->id
        ];
        Form::InsertAttachment('add',$dataInsert);

        TempUpload::where(['form_number'=>$formNumber,'uuid'=>$item->uuid])->update(['is_updated'=>1,'updated_at'=>$tanggal]);
      }

			$listCC=[];
			if(count($location)>0) 
			{
				foreach ($location as $row) {
					$dataInsert=[
						'PT_ID'=>$tableId,
						'ID_LOCATION_PREFERRED'=>$row,
						'AKTIF_YN'=>'Y',
						'USER_ENTRY'=>'ONLINE',
						'TGL_ENTRY'=>$tanggal,
						'TGL_UPDATE'=>$tanggal
					];
					Form::InsertLocation('add',$dataInsert);	
					//Add New CC
					$dataEmail=Form::getLocation(['ID_LOCATION_PREFERRED'=>$row])->first();
					if($dataEmail->EMAIL_LSM != '')
						$listCC=array_merge($listCC,[$dataEmail->EMAIL_LSM]);
				}
			}

			$params=['email'=>$email,'name'=>$brand,'list_cc'=>$listCC];
      
      // Mail::send('site.mail.submit', [], function ($m) use ($params) {
      //   $m->from('no-reply@summarecon.com', 'Leasing Online Summarecon');
      //   $m->to($params['email'], $params['name']);
      //   $m->subject('Leasing Online Summarecon');
      //   if(count($params['list_cc'])>0) {
      //     foreach ($params['list_cc'] as $row) {
      //       $m->bcc($row);
      //     }
      //   }
      // });
      DB::commit();

      $this->_request->session()->put('submit-sukses','Data Berhasil Disimpan !');
			return response()->json(['status'=>'1','msg'=>'Sukses']);

      //$this->_request->session()->put('submit-sukses','Data Berhasil Disimpan !');
      //return redirect('/');
		} 
		catch (\Exception $e) 
		{
      DB::rollback();
			$this->_request->session()->put('submit-error',$e->getMessage());
			//return back()->withInput();
			return response()->json(['status'=>'0','msg'=>$e->getMessage()]);
		}
	
  }
  
  public function upload_attach()
  {
    $file = $this->_request->file('file');
    $formNumber = trim($this->_request->input('form_number'));
    $uuid = trim($this->_request->input('uuid'));
    $brand = trim($this->_request->input('brand'));

    if($brand=='') return response()->json(['status'=>'0','error'=>'Nama Brand Harus Diisi']);

    // Upload File
    $destinationPath=base_path('assets/files/attachment');
    // $name=str_slug($file->getClientOriginalName());
    $extension=$file->getClientOriginalExtension();
    $fileName=str_slug($brand).'-'.str_random(5).'.'.$extension;
    $file->move($destinationPath, $fileName);

    // Insert Database
    $recFiles = new TempUpload;
    $recFiles->form_number = $formNumber;
    $recFiles->uuid = $uuid;
    $recFiles->file_name = $fileName;
    $recFiles->is_updated = 0;
    $recFiles->created_at = date('Y-m-d H:i:s');
    $recFiles->save();

    return response()->json(['status'=>'1','uuid'=>$uuid]);
  }

  public function delete_attach()
  {
    $formNumber = trim($this->_request->input('form_number'));
    $uuid = trim($this->_request->input('uuid'));
    // Check Data
    $recFiles = TempUpload::where(['form_number'=>$formNumber,'uuid'=>$uuid])->first();
    if($recFiles) {
      $fileName = $recFiles->file_name;
      if( file_exists(base_path('assets/files/attachment/'.$fileName)) ) {
        unlink(base_path('assets/files/attachment/'.$fileName));
      }
      TempUpload::where(['form_number'=>$formNumber,'uuid'=>$uuid])->delete();
    }
  }

  public function test_attach()
  {
    $recAttach = \DB::table('PM_PROSPECTIVE_TENANT_V2 As a')->join('PM_PROSPECTIVE_TENANT_ATTCH As b','a.PT_ID','=','b.PT_ID')
                  ->where('PT_TGL_ENTRY','>=','2021-02-15')->select('a.PT_ID','PT_TRADE_NAME','TMP_FORM_NUMBER','ATTACHMENT')->get();
    
    foreach ($recAttach as $key => $item) {
      if(!file_exists(base_path('assets/files/attachment/'.$item->ATTACHMENT))) {
        print($item->ATTACHMENT).' '.$item->PT_ID.'<br>';
      }
    }
  }

}