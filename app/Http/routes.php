<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','Form\FormController@index');
Route::post('/get-class','Form\FormController@getClass');

Route::post('submit-online-form','Form\FormController@submit');

// Upload Temp
Route::post('upload-attachement','Form\FormController@upload_attach');

// Delete Upload Temp
Route::post('delete-attachement','Form\FormController@delete_attach');

Route::get('test-attach','Form\FormController@test_attach');