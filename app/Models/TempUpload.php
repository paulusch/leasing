<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TempUpload extends Model
{
    public $timestamps = false;
    protected $table = 'temp_file_uploads';
}
