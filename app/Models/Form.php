<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Form extends Model
{
    public $timestamps = false;
    public $primaryKey = 'PT_ID';
    protected $table = 'PM_PROSPECTIVE_TENANT_V2';

    public static function getBisChain($where=[],$order='',$orderBy='asc')
    {
        $data = DB::table('PM_PROSPECTIVE_TENANT_BUSINESS_CHAIN');

        if(count($where) > 0)
            $data = $data->where($where);
        else
            $data = $data->where(['AKTIF_YN'=>'Y']);

        if(isset($order) && $order != '')
            $data = $data->orderBy($order,$orderBy);

        return $data;
    }

    public static function getCategory($where=[],$order='',$orderBy='asc')
    {
        $data = DB::table('PM_PROSPECTIVE_TENANT_KATEGORI');

        if(count($where) > 0)
            $data = $data->where($where);
        else
            $data = $data->where(['FLAG_AKTIF'=>'A']);

        if(isset($order) && $order != '')
            $data = $data->orderBy($order,$orderBy);

        return $data;
    }

    public static function getCategoryDetail($where=[],$order='',$orderBy='asc')
    {
        $data = DB::table('PM_PROSPECTIVE_TENANT_KATEGORI_RETAIL');

        if(count($where) > 0)
            $data = $data->where($where);
        else
            $data = $data->where(['FLAG_AKTIF'=>'A']);

        if(isset($order) && $order != '')
            $data = $data->orderBy($order,$orderBy);

        return $data;
    }

    public static function getGroup($where=[],$order='',$orderBy='asc')
    {
        $data = DB::table('PM_NASABAH');

        if(count($where) > 0)
            $data = $data->where($where);
        else
            $data = $data->where(['FLAG_AKTIF'=>'A']);

        if(isset($order) && $order != '')
            $data = $data->orderBy($order,$orderBy);

        return $data;
    }

    public static function getLocation($where=[],$order='',$orderBy='asc')
    {
        $data = DB::table('PM_LOCATION_PREFERRED');

        if(count($where) > 0)
            $data = $data->where($where);
        else
            $data = $data->where(['FLAG_AKTIF'=>'Y']);

        if(isset($order) && $order != '')
            $data = $data->orderBy($order,$orderBy);

        return $data;
    }

    public static function updateData($mode='',$params=[],$where=[])
    {
        if($mode=='add')
            return DB::table('PM_PROSPECTIVE_TENANT_V2')->insert($params);    
        elseif($mode=='insert_id')
            return DB::table('PM_PROSPECTIVE_TENANT_V2')->insertGetId($params);  
    }

    public static function InsertLocation($mode='',$params=[],$where=[])
    {
        if($mode=='add')
            return DB::table('PM_PROSPECTIVE_TENANT_LOCATION_PREFERRED')->insert($params);    
        elseif($mode=='insert_id')
            return DB::table('PM_PROSPECTIVE_TENANT_LOCATION_PREFERRED')->insertGetId($params);  
    }

    public static function InsertAttachment($mode='',$params=[],$where=[])
    {
        if($mode=='add')
            return DB::table('PM_PROSPECTIVE_TENANT_ATTCH')->insert($params);    
        elseif($mode=='insert_id')
            return DB::table('PM_PROSPECTIVE_TENANT_ATTCH')->insertGetId($params);  
    }
}
